import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';

@Injectable({ providedIn: 'root' })
export class CookiesService {

  private exdays = environment.cookiesExpiresIn;

  deleteCookie(cname) {
    let expires = "expires=Thu, 01 Jan 1970 00:00:00 UTC" + ";path=/";
    document.cookie = cname + "=;" + expires + ";path=/";
  }

  setCookie(cname, cvalue) {
    let d = new Date();
    d.setTime(d.getTime() + (this.exdays * 24 * 60 * 60 * 1000));
    let expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  getCookie(cname) {
    let name = cname + "=";
    let ca = document.cookie.split(';');
    for(let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  checkCookie(cname) {
    let user = this.getCookie(cname);
    if (user != "") {
     return true;
    } else {
      return false;
    }
  }

}

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {AuthService} from './auth.service';

@Injectable({ providedIn: 'root' })
export class DbService {

  private apiUrl = environment.apiUrl;

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
  ) {
  }

  login(username, password) {

    let httpParams = new HttpParams();
    httpParams = httpParams.append('a', 'login');
    httpParams = httpParams.append('username', username);
    httpParams = httpParams.append('password', password);

    return this.httpClient.post(this.apiUrl, httpParams, { observe: 'response' });
  }

  register(username, password) {

    let httpParams = new HttpParams();
    httpParams = httpParams.append('a', 'register');
    httpParams = httpParams.append('username', username);
    httpParams = httpParams.append('password', password);

    return this.httpClient.post(this.apiUrl, httpParams, { observe: 'response' });
  }

  getPeople() {

    let httpHeaders = new HttpHeaders();
    httpHeaders = httpHeaders.append('userdata', JSON.stringify(this.authService.getUserData()));

    let httpParams = new HttpParams();
    httpParams = httpParams.append('a', 'friends');

    return this.httpClient.get(this.apiUrl,
      { params: httpParams, headers: httpHeaders, observe: 'response' });
  }

}

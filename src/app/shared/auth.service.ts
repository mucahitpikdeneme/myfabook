import {Injectable} from '@angular/core';
import {CookiesService} from './cookies.service';

@Injectable({ providedIn: 'root' })
export class AuthService {

  constructor(
    private cookeisService: CookiesService
  ) {
  }

  isLoggedIn() {
    return this.cookeisService.checkCookie('userData');
  }

  getUserData() {
    const userData = this.cookeisService.getCookie('userData');
    return JSON.parse(userData);
  }

  loggedIn(userdata) {
    return this.cookeisService.setCookie('userData', JSON.stringify(userdata));
  }

  loggedOut() {
    return this.cookeisService.deleteCookie('userData');
  }

  emailValite(email) {
    if (/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        .test(email)
      === true )
    { return true; }
    else { return false; }
  }

}


import {Pipe, PipeTransform} from '@angular/core';
import {AuthService} from '../auth.service';

@Pipe({
  name: 'isAuth',
  pure: false,
})
export class AuthPipe implements PipeTransform {

  constructor(
    private authService: AuthService
  ) {
  }

  transform(value: any, field: any){
    if  (field === 'username') { return this.authService.getUserData().username; }
    else { return this.authService.isLoggedIn(); }

  }
}

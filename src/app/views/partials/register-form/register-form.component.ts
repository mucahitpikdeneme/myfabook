import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
})
export class RegisterFormComponent implements OnInit {
  @Output() submitEE = new EventEmitter<NgForm>();

  pending = false;
  error = '';

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    this.pending = true;
    this.submitEE.next(form);
  }

  onClickSubmit() {

  }

}

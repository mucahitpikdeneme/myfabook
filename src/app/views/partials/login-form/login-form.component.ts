import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
})
export class LoginFormComponent implements OnInit {

  @Output() submitEE = new EventEmitter<NgForm>();

  pending = false;
  error = '';

  constructor() { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {
    this.pending = true;
     this.submitEE.next(form);
  }

  onClickSubmit() {

  }

}


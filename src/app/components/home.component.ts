import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {DbService} from '../shared/db.service';
import {LoginFormComponent} from '../views/partials/login-form/login-form.component';
import {HomePageComponent} from '../views/partials/home-page/home-page.component';

@Component({
  selector: 'app-login',
  templateUrl: '../views/main.html'
})
export class HomeComponent implements OnInit {

  @ViewChild(HomePageComponent) homePageComponent: HomePageComponent;

  viewParams: any = {
    homePage: true,
  }

  constructor(
    private dbService: DbService
  ) {
  }

  ngOnInit() {
  }

}

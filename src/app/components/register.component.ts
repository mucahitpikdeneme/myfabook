import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {DbService} from '../shared/db.service';
import {AuthService} from '../shared/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RegisterFormComponent} from '../views/partials/register-form/register-form.component';

@Component({
  selector: 'app-login',
  templateUrl: '../views/main.html'
})
export class RegisterComponent implements OnInit {

  @ViewChild(RegisterFormComponent) registerFormComponent: RegisterFormComponent;

  viewParams: any = {
    register: true
  }

  constructor(
    private dbService: DbService,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    if (this.authService.isLoggedIn()) this.router.navigate(['in/home'],{ relativeTo: this.activatedRoute });
  }

  onSubmit(form: NgForm) {
   this.registerFormComponent.error = '';

   if (!form.value.username || !form.value.password || !form.value.password2) {
     this.registerFormComponent.error = 'Please, fill all text areaas!';
     this.registerFormComponent.pending = false;
     return;
   }
   else if ((form.value.username.length < 3)) {
     this.registerFormComponent.error = 'Username must have more than 3 characters!';
     this.registerFormComponent.pending = false;
     return;
   }
   else if (form.value.password !== form.value.password2) {
     this.registerFormComponent.error = 'Passwords not mathing!';
     this.registerFormComponent.pending = false;
     return;
   }

    this.dbService.register(form.value.username, form.value.password)
      .subscribe(response => {
        const body: any = response.body;
        if (body.success) {
          this.registerFormComponent.error = 'Registration Successfull!';
          form.resetForm();
          setTimeout(() => {
            this.authService.loggedIn({
              id: body.id,
              username: body.username,
              friends: body.friends,
              lastLogin: body.lastLogin,
            });
            this.router.navigate(['../in/home'],{ relativeTo: this.activatedRoute });
          });
        } else {
          this.registerFormComponent.error = form.value.username + ' is already registered!';
          form.resetForm();
        }
        this.registerFormComponent.pending = false;
      });/* */
  }

}

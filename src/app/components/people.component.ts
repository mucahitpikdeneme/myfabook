import {Component, OnInit, ViewChild} from '@angular/core';
import {LoginFormComponent} from '../views/partials/login-form/login-form.component';
import {AuthService} from '../shared/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import {PeoplePageComponent} from '../views/partials/people-page/people-page.component';
import {HomePageComponent} from '../views/partials/home-page/home-page.component';
import {DbService} from '../shared/db.service';

@Component({
  selector: 'app-people',
  templateUrl: '../views/main.html'
})
export class PeopleComponent implements OnInit {

  @ViewChild(PeoplePageComponent) peoplePageComponent: PeoplePageComponent;

  viewParams: any = {
    peoplePage: true,
  }

  people: any = [];

  constructor(
    private dbService: DbService
  ) {
  }

  ngOnInit() {
    this.dbService.getPeople()
      .subscribe(res => {
        const body: any = res.body;
        this.people = this.peoplePageComponent.people = body.people;
      });
  }

}


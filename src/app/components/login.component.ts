import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from '@angular/forms';
import {DbService} from '../shared/db.service';
import {LoginFormComponent} from '../views/partials/login-form/login-form.component';
import {AuthService} from '../shared/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: '../views/main.html'
})
export class LoginComponent implements OnInit {

  @ViewChild(LoginFormComponent) loginFormComponent: LoginFormComponent;

  viewParams: any = {
    login: true
  }

  constructor(
    private dbService: DbService,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    if (this.authService.isLoggedIn()) this.router.navigate(['in/home'],{ relativeTo: this.activatedRoute });
  }

  onSubmit(form: NgForm) {
    this.loginFormComponent.error = '';

    this.dbService.login(form.value.username, form.value.password)
      .subscribe(response => {
        const body: any = response.body;
        if (body.success) {
          this.loginFormComponent.error = 'Login Successfull!';
          form.resetForm();
          setTimeout(() => {
            this.authService.loggedIn({
              id: body.id,
              username: body.username,
              friends: body.friends,
              lastLogin: body.lastLogin,
            });
            this.router.navigate(['in/home'],{ relativeTo: this.activatedRoute });
          });
        } else {
          this.loginFormComponent.error = 'E-mail or password invalid';
          form.resetForm();
        }
        this.loginFormComponent.pending = false;
      });
  }

}

import {Component, OnInit, ViewChild} from '@angular/core';
import {LoginFormComponent} from '../views/partials/login-form/login-form.component';
import {AuthService} from '../shared/auth.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: '../views/main.html'
})
export class LogoutComponent implements OnInit {

  @ViewChild(LoginFormComponent) loginFormComponent: LoginFormComponent;

  viewParams: any = {
  }

  constructor(
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit() {
    this.authService.loggedOut();
    this.router.navigate(['../'],{ relativeTo: this.activatedRoute });
  }

}

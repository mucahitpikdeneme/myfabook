import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {LoginComponent} from './components/login.component';
import {appRoutingModule} from './app-routing.module';
import {LoginFormComponent} from './views/partials/login-form/login-form.component';
import {HomePageComponent} from './views/partials/home-page/home-page.component';
import {HomeComponent} from './components/home.component';
import {AuthPipe} from './shared/Pipes/Auth.pipe';
import {LogoutComponent} from './components/logout.component';
import {RegisterComponent} from './components/register.component';
import { RegisterFormComponent } from './views/partials/register-form/register-form.component';
import { PeoplePageComponent } from './views/partials/people-page/people-page.component';
import {PeopleComponent} from './components/people.component';

@NgModule({
  declarations: [
    AppComponent,
    // ImupComponent,
    LoginComponent,
    RegisterComponent,
    LogoutComponent,
    LoginFormComponent,
    HomePageComponent,
    HomeComponent,
    AuthPipe,
    RegisterFormComponent,
    PeoplePageComponent,
    PeopleComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    appRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

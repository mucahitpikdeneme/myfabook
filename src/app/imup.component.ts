import {Component} from '@angular/core';
import {HttpClient, HttpEventType} from '@angular/common/http';

@Component({
  selector: 'app-imup',
  template: '' +
    '<input type="file" (change)="fileSelected($event)">' +
    '',
})
export class ImupComponent {

  constructor(
    private httpClient: HttpClient
  ) {
  }

  fileSelected(event) {

    const file = event.target.files[0];

    let formData = new FormData();

    formData.append('thumbnail', file);


    this.httpClient.post('http://localhost/education/angular_projects/memtasks/fileupload.php', formData, {
      observe: 'events', reportProgress: true
    })
      .subscribe(events => {
        if (events.type == HttpEventType.UploadProgress) {
          const per = Math.round(100 * events.loaded / events.total);
          console.log(per);
        }
    });
  }

}

import {NgModule} from '@angular/core';
import {LoginComponent} from './components/login.component';
import {RouterModule} from '@angular/router';
import {HomeComponent} from './components/home.component';
import {AuthGuardService} from './shared/AuthGuard.service';
import {LogoutComponent} from './components/logout.component';
import {RegisterComponent} from './components/register.component';
import {PeopleComponent} from './components/people.component';

const routes = [
  { path: '', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'in', children: [
      { path: 'home', component: HomeComponent },
      { path: 'people', component: PeopleComponent },
    ], canActivate: [AuthGuardService]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class appRoutingModule {}
